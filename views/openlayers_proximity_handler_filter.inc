<?php 

class openlayers_proximity_handler_filter extends views_handler_filter_float {

  /**
   * Display the filter on the administrative summary
   */
  function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    $options = $this->operator_options('short');
    $output = check_plain($options[$this->operator]);
    if (in_array($this->operator, $this->operator_values(2))) {
      $output .= ' ' . t('@min and @max @unit from @location', array('@min' => $this->value['min'], '@max' => $this->value['max'], '@unit' => $this->value['unit'], '@location' => $this->value['location']));
    }
    elseif (in_array($this->operator, $this->operator_values(1))) {
      $output .= ' ' . t('@value @unit from @location', array('@value' => $this->value['value'], '@unit' => $this->value['unit'], '@location' => $this->value['location']));
    }
    return $output;
  }  

  /**
   * Information about options for all kinds of purposes will be held here.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['operator'] = array('default' => '<');
    $options['value']['unit'] = array('default' => variable_get('openlayers_proximity_unit', OPENLAYERS_PROXIMITY_DEFAULT_UNIT));
    $options['value']['location'] = array('default' => '');
    return $options;
  }  

  /**
   * Provide default options for exposed filters.
   */
  function expose_options() {
    parent::expose_options();
    $this->options['expose']['use_unit'] = FALSE;
    $this->options['expose']['unit'] = 'unit';
    $this->options['expose']['use_location'] = FALSE; 
    $this->options['expose']['location'] = 'location';
  }

  /**
   * Handle the 'left' side fo the exposed options form.
   */
  function expose_form_left(&$form, &$form_state) {
    
    $form['expose']['use_unit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Unlock unit of measurement'),
      '#default_value' => $this->options['expose']['use_unit'],
      '#description' => t('When checked, unit of measurement will be exposed to the user'),
    );
    $form['expose']['unit'] = array(
      '#type' => 'textfield',
      '#default_value' => $this->options['expose']['unit'],
      '#title' => t('Unit of measurement identifier'),
      '#size' => 40,
      '#description' => t('This will appear in the URL after the ? to identify this operator.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-options-expose-use-unit' => array(1)
      ),
    );

    $form['expose']['use_location'] = array(
      '#type' => 'checkbox',
      '#title' => t('Unlock location'),
      '#default_value' => $this->options['expose']['use_location'],
      '#description' => t('When checked, location will be exposed to the user'),
    );
    $form['expose']['location'] = array(
      '#type' => 'textfield',
      '#default_value' => $this->options['expose']['location'],
      '#title' => t('Location identifier'),
      '#size' => 40,
      '#description' => t('This will appear in the URL after the ? to identify this operator.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-options-expose-use-location' => array(1)
      ),
    );
    parent::expose_form_left($form, $form_state);
  }

  /**
   * Provide a simple textfield for equality
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    
    
    $form['value']['wrapper_open']['#value'] = '<div class="dependent-options">';
    $form['value']['location'] = array(
      '#title' => t('Location'),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $this->options['value']['location'],
      '#description' => t('Location where to start the search from.'),
      '#prefix' => '<div class="views-right-70">',
      '#suffix' => '</div>',
    );
    $form['value']['unit'] = array(
      '#title' => t('Unit of measurement'),
      '#type' => 'select',
      '#options' => openlayers_proximity_get_available_units(),
      '#default_value' => $this->options['value']['unit'],
      '#description' => t('Select unit of measurement.'),
      '#prefix' => '<div class="views-left-30">',
      '#suffix' => '</div>',
    );
    $form['value']['wrapper_close']['#value'] = '</div>';
    
  }
  
  /**
   * Provide a form for setting the operator.
   */
  function operator_form(&$form, &$form_state) {
    parent::operator_form($form, $form_state);
    
    // At the end of the day proximity search is a rough estimation.  
    unset($form['operator']['#options']['<=']);
    unset($form['operator']['#options']['=']);
    unset($form['operator']['#options']['!=']);
    unset($form['operator']['#options']['>=']);
    
    // Why you would ask yourself that?
    unset($form['operator']['#options']['not between']);
  }
  
  /**
   * Render our chunk of the exposed filter form when selecting
   */
  function exposed_form(&$form, &$form_state) {
    
    if (empty($this->options['exposed'])) {
      return;
    }
    
    $force_operator = FALSE;
    if (empty($this->options['expose']['use_operator'])) {
      $this->options['expose']['use_operator'] = TRUE;
      $force_operator = TRUE;
    }
    parent::exposed_form($form, $form_state);
    
    if ($force_operator) {
      $operator = $this->options['expose']['operator'];
      $form[$operator]['#type'] = 'hidden';
      $form[$operator]['#value'] = $form[$operator]['#default_value']; 
      unset($form[$operator]['#options']);
    }
    
    // When exposed pull location and unit out of value form item.
    if (!empty($this->options['expose']['identifier'])) {
      $identifier = $this->options['expose']['identifier'];
      unset($form[$identifier]['wrapper_open']);
      unset($form[$identifier]['wrapper_close']);
      $form[$identifier]['#type'] = 'fieldset';
      $form[$identifier]['#attributes'] = array('class' => "openlayers-proximity-filter-exposed");
      
      
      // Expose unit of measurement form, if necessary.
      $exposing_unit = FALSE;
      if (!empty($this->options['expose']['use_unit']) && !empty($this->options['expose']['unit'])) {
        // Fetch unit identifier.
        $exposing_unit = TRUE;
        $value = $this->options['expose']['unit'];
        if ($value != 'unit') {
          $form[$identifier][$value] = $form[$identifier]['unit'];
          unset($form[$identifier]['unit']); 
        }
        // Clean up exposed form.
        unset($form[$identifier][$value]['#prefix']);
        unset($form[$identifier][$value]['#suffix']);
        unset($form[$identifier][$value]['#title']);
        unset($form[$identifier][$value]['#description']);
      }
      else {
        unset($form[$identifier]['unit']);  
      }
        
      
      // Expose location form, if necessary.
      $location = '';
      if (!empty($this->options['expose']['use_location']) && !empty($this->options['expose']['location'])) {
        // Fetch location identifier.
        $value = $this->options['expose']['location'];
        if ($value != 'location') {
          $form[$identifier][$value] = $form[$identifier]['location'];
          unset($form[$identifier]['location']); 
        }
        // Clean up exposed form.
        unset($form[$identifier][$value]['#prefix']);
        unset($form[$identifier][$value]['#suffix']);
        unset($form[$identifier][$value]['#title']);
        unset($form[$identifier][$value]['#description']);
        $form[$identifier][$value]['#field_prefix'] = $exposing_unit ? t('from') : '';
      }
      else {
        unset($form[$identifier]['location']);
        $location = $this->options['value']['location'];   
        $form[$identifier]['location']['#value'] = $exposing_unit ? '<div class=" form-item">'. t('@unit from @location', array('@unit' => $units[$this->options['value']['unit']], '@location' => $location)) .'</div>' : '';
      }        
      
      $operators = $this->operator_options();
      $units = openlayers_proximity_get_available_units();
      
      // Add meaningful suffix to value.
      if (isset($form[$identifier]['value'])) {
        $form[$identifier]['value']['#field_prefix'] = $force_operator ? $operators[$this->options['operator']] : '';
        $form[$identifier]['value']['#field_suffix'] = $exposing_unit ? '' : t('@unit from @location', array('@unit' => $units[$this->options['value']['unit']], '@location' => $location));
        $form[$identifier]['value']['#size'] = 3;
      }
      
      // Add meaningful prefix/suffix to min max.
      if (isset($form[$identifier]['min']) && isset($form[$identifier]['max'])) {
        $form[$identifier]['min']['#field_prefix'] = $force_operator ? $operators[$this->options['operator']] : '';
        $form[$identifier]['min']['#size'] = 3;
        $form[$identifier]['max']['#size'] = 3;
        unset($form[$identifier]['max']['#title']);
        $form[$identifier]['max']['#field_prefix'] = t('and');
        $form[$identifier]['max']['#field_suffix'] = t('@unit from @location', array('@unit' => $units[$this->options['value']['unit']], '@location' => $location));
      }
    }
    
  }
  
  // @TODO: Implement this.
  function op_between($field) {
    $this->op_process('between', $field);
  }

  function op_simple($field) {
    $this->op_process('simple', $field);
  }
  
  function op_process($op = 'simple', $field) {

    // Fallback on filter options.
    $value = isset($this->value['value']) ? $this->value['value'] : $this->options['value']['value'];
    $min = isset($this->value['min']) ? $this->value['min'] : $this->options['value']['min'];
    $max = isset($this->value['max']) ? $this->value['max'] : $this->options['value']['max'];
    $location = isset($this->value[$this->options['expose']['location']]) ? $this->value[$this->options['expose']['location']] : $this->options['value']['location'];
    $unit = isset($this->value[$this->options['expose']['unit']]) ? $this->value[$this->options['expose']['unit']] : $this->options['value']['unit'];

    $table = $this->query->get_table_info('openlayers_proximity');
    $lat = $this->table_alias .'.lat';
    $lon = $this->table_alias .'.lon';
    
    if ($response = openlayers_geocoder_response($location)) {
      
      if ($op == 'simple') {
        
        $radius = openlayers_proximity_measurement_units_convert($unit, $value);
        $bounds = openlayers_proximity_get_bounds($response[0]['location']['lat'], $response[0]['location']['lng'], $radius);
        $where = openlayers_proximity_get_where_clause($this->operator, $bounds, $this->table_alias);        
      }
      else {
        
        $radius = openlayers_proximity_measurement_units_convert($unit, $max);
        $bounds = openlayers_proximity_get_bounds($response[0]['location']['lat'], $response[0]['location']['lng'], $radius);
        $inner = openlayers_proximity_get_where_clause('<', $bounds, $this->table_alias);
        
        $radius = openlayers_proximity_measurement_units_convert($unit, $min);
        $bounds = openlayers_proximity_get_bounds($response[0]['location']['lat'], $response[0]['location']['lng'], $radius);
        $outer = openlayers_proximity_get_where_clause('>', $bounds, $this->table_alias);
          
        $where = "($inner AND $outer)";
      }
      
      $this->query->add_where($this->options['group'], $where);
    }
    $this->query->add_groupby("nid");
  }
  
}