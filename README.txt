
# OpenLayers Proximity

OpenLayers Proximity enables geographical proximity search for the OpenLayers 
module.

# Views integration

The module exposes a filter to the Views module where users can specify 
starting point and radius of the proximity search. The starting point can be 
specified by simply providing the name of the location we want to center our 
search to. Search radius can be specified in kilometers or miles and it's 
easy, for external modules, to specify their own units of measurement. 

# Configuration 

After enabling the module is necessary to build the proximity index table by
visiting "admin/build/openlayers/proximity". This has to be done only the 
first time since it will index all existing nodes, the module will 
automatically index newly created nodes.